/*
 * 配置自定义菜单
 */
'use strict'

module.exports = {
	
 	'button':[
 	{	
    'type':'click',
    'name':'最新',
    'key':'menu_click',
   /* 'sub_button':[]*/
  },
  {
    'name':'点出菜单',
    'sub_button':[
    {	
      'name':'跳转',
      'type':'view',
      'url':'http://www.baidu.com'
    },
    {	
      'name':'扫码推送事件',
      'type':'scancode_push',
      'key':'qr_scan'
    },
    {	
      'name':'扫码推送中',
      'type':'scancode_waitmsg',
      'key':'qr_scan_wait'
    },
    {	
      'name':'弹出系统拍照',
      'type':'pic_sysphoto',
      'key':'pic_photo'
    },
    {	
      'name':'弹出系统拍照或者相册',
      'type':'pic_photo_or_album',
      'key':'pic_photo_album'
    }]
 	},
 	{
    'name':'点出菜单2',
    'sub_button':[
    {	
     'type':'pic_weixin',
      'name':'微信相册发图',
      'key':'pic_weixin'
    },
    { 
      'type':'location_select',
      'name':'地理位置选择',
      'key':'location_select',
    },
    /*{
      'type':'media_id',
      'name':'下发图片消息',
      'media_id':'xxx',
    },
    {
      'type':'viwe_limited',
      'name':'跳转图文消息的URL',
      'media_id':'xxx',
    }*/]
 	}]
}