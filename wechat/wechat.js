'use strict'

var fs = require('fs');
var Promise = require('bluebird');//导入这个模块来调用Promise，来实现数据继续往下传
var _ = require('lodash');
var request = Promise.promisify(require('request'));//因为我们用到了Promise，所以在调用request的时候需要这样导入
var util = require('./util');
var prefix = 'https://api.weixin.qq.com/cgi-bin/';//因为这一部分API是固定的，所以我们单独拿出来
var mpPrefix = 'https://mp.weixin.qq.com/cgi-bin/';
var semanticUrl = 'https://api.weixin.qq.com/semanticsemproxy/search?';
var api = {
    semanticUrl: semanticUrl,
    accessToken:prefix+'token?grant_type=client_credential',
    uploadTempMaterial:prefix+'media/upload?',  //access_token=ACCESS_TOKEN&type=TYPE  上传临时素材
    getTempMaterial:prefix+'media/get?',        //access_token=ACCESS_TOKEN&media_id=MEDIA_ID 获取临时素材，GET请求
    uploadPermNews:prefix+'material/add_news?',   //access_token=ACCESS_TOKEN  上传永久图文
    uploadPermPics:prefix+'media/uploadimg?',   //access_token=ACCESS_TOKEN  上传永久图片
    uploadPermOther:prefix+'material/add_material?',   //access_token=ACCESS_TOKEN  上传永久其他素材
    getPermMaterial:prefix+'material/get_material?',   //access_token=ACCESS_TOKEN 获取永久素材，POST请求
    delPermMaterial:prefix+'material/del_material?',   //access_token=ACCESS_TOKEN 删除永久素材，POST请求
    updatePermMaterial:prefix+'material/update_news?',   //access_token=ACCESS_TOKEN 更新永久素材，POST请求
    count: prefix + 'material/get_materialcount?',
    batch: prefix + 'material/batchget_materialcount?',
    temporary: {
        uploadMaterial : prefix + 'media/upload?'
    },
    permanent: {
         uploadMaterial : prefix + 'material/add_material?',
         uploadNews: prefix + 'material/add_news?',
         uploadNewsPic: prefix + 'material/add_news?'
    },
    menu:{
        create:prefix+'menu/create?',  //access_token=ACCESS_TOKEN  创建菜单
        get:prefix+'menu/get?',        //access_token=ACCESS_TOKE  获取菜单,GET请求
        del:prefix+'menu/delete?',  //access_token=ACCESS_TOKEN  删除菜单,GET请求
        current:prefix+'get_current_selfmenu_info?'  //access_token=ACCESS_TOKEN  获取自定义菜单配置接口
    },
    groups:{
        create:prefix+'groups/create?',  //access_token=ACCESS_TOKEN  创建分组，POST请求
        fetch:prefix+'groups/get?',        //access_token=ACCESS_TOKE  查询所有分组,GET请求
        check:prefix+'groups/getid?',    //access_token=ACCESS_TOKEN  查询用户所在分组,POST请求
        update:prefix+'groups/update?',  //access_token=ACCESS_TOKEN  修改分组名,POST请求
        move:prefix+'groups/members/update?',  //access_token=ACCESS_TOKEN  移动用户分组,POST请求
        batchupdate:prefix+'groups/members/batchupdate?', //access_token=ACCESS_TOKEN  批量移动用户分组,POST请求
        del:prefix+'groups/delete?'   //access_token=ACCESS_TOKEN    删除分组,POST请求
    },
    user:{
        remark:prefix+'user/info/updateremark?',  //access_token=ACCESS_TOKEN  修改用户备注名，POST请求
        fetch:prefix+'user/info?', //access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN  获取用户基本信息，GET请求
        batchFetch:prefix+'user/info/batchget?',  //access_token=ACCESS_TOKEN，POST请求
        list:prefix+'user/get?',  //access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID，GET请求
    },
    mass:{
        group:prefix+'message/mass/sendall?',  //access_token=ACCESS_TOKEN 群发消息
        openId: prefix + 'message/mass/send?',
        del: prefix + 'message/mass/delete?',
        preview: prefix + 'message/mass/preview?',
        check: prefix + 'message/mass/get?',
    },
   qrcode:{
    create:prefix + 'qrcode/create?',
    show: mpPrefix + 'showqrcode?'
   },
   shorturl:{
    create: prefix + 'shorturl?'
   },
   ticket: {
    get: prefix + 'ticket/getticket?'
   }
}

function Wechat(opts) { //这里面的值就是从中间件传过来的
    this.appID = opts.appID;
    this.appSecret = opts.appSecret;
    this.getAccessToken = opts.getAccessToken;
    this.saveAccessToken = opts.saveAccessToken;
    this.getTicket = opts.getTicket;
    this.saveTicket = opts.saveTicket;
    this.fetchAccessToken();
}
//为这个对象添加我们需要的函数

Wechat.prototype.fetchAccessToken = function(data){
    var that = this;

    // 如果this上已经存在有效的access_token，直接返回this对象
    if(this.access_token && this.expires_in){
        if(this.isValidAccessToken(this)){
            return Promise.resolve(this);
        }
    }

    return this.getAccessToken().then(function(data){
        try{
            data = JSON.parse(data);
        }catch(e){
            return that.updateAccessToken();
        }

        if(that.isValidAccessToken(data)){
            return Promise.resolve(data);
        }else{
            return that.updateAccessToken();
        }
    }).then(function(data){
        that.access_token = data.access_token;
        that.expires_in = data.expires_in;
        that.saveAccessToken(JSON.stringify(data));
        return Promise.resolve(data);
    });
}

Wechat.prototype.fetchTicket = function(access_token){
    var that = this;

    return this.getTicket().then(function(data){
        try{
            data = JSON.parse(data);
        }catch(e){
            return that.updateTicket(access_token);
        }

        if(that.isValidTicket(data)){
            return Promise.resolve(data);
        }else{
            return that.updateTicket(access_token);
        }
    }).then(function(data){

        that.saveTicket(data);
        return Promise.resolve(data);
    });
}

Wechat.prototype.isValidAccessToken = function (data) {
    if (!data || !data.access_token || !data.expires_in) {
        return false;
    }
    var access_token = data.access_token;
    var expires_in = data.expires_in;
    var now = (new Date().getTime())

    if (now < expires_in) {
        return true;
    }else {
        return false;
    }
}
Wechat.prototype.updateAccessToken = function () {
    var appID = this.appID;
    var appSecret = this.appSecret;
    var url = api.accessToken + '&appid=' + appID + '&secret=' + appSecret;
    return new Promise(function (resolve, reject) {
        request({url: url, json: true}, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                var data = body;
                var now = (new Date().getTime());
                var expires_in = now + (data.expires_in - 20) * 1000;
                data.expires_in = expires_in;
                resolve(data);
               // console.log(data);
            } else {
                reject()
            }
        });
    })
}

Wechat.prototype.isValidTicket = function (data) {
    if (!data || !data.ticket || !data.expires_in) {
        return false;
    }
    var ticket = data.ticket;
    var expires_in = data.expires_in;
    var now = (new Date().getTime())

    if (ticket && now < expires_in) {
        return true;
    }else {
        return false;
    }
}

Wechat.prototype.updateTicket = function () {
    var access_token = this.access_token;
    var url = api.ticket.get + '&access_token=' + access_token + '&type=jsapi';
    return new Promise(function (resolve, reject) {
        request({url: url, json: true}, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                var data = body;
                var now = (new Date().getTime());
                var expires_in = now + (data.expires_in - 20) * 1000;
                data.expires_in = expires_in;
                resolve(data);
               // console.log(data);
            } else {
                reject()
            }
        });
    })
}

Wechat.prototype.uploadTempMaterial = function(type,filepath){
    var that = this;
    var form = {  //构造表单
        media:fs.createReadStream(filepath)
    }
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.uploadTempMaterial + 'access_token=' + data.access_token + '&type=' + type;
            request({url:url,method:'POST',formData:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data)
                }else{
                    throw new Error('upload temporary material failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.uploadPermMaterial = function(type,material){
    var that = this;
    var form = {}
    var uploadUrl = '';
    if(type === 'pic') uploadUrl = api.uploadPermPics;
    if(type === 'other') uploadUrl = api.uploadPermOther;
    if(type === 'news'){
        uploadUrl = api.uploadPermNews;
        form = material
    }else{
        form.media = fs.createReadStream(material);
    }
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = uploadUrl + 'access_token=' + data.access_token;
            var opts = {
                method:'POST',
                url:url,
                json:true
            }
            if( type === 'news'){
                opts.body = form;
            }else{
                opts.formData = form;
            }
            request(opts).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('upload permanent material failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.getMaterial = function(mediaId,permanent){
    var that = this;
    var getUrl = permanent ? api.getPermMaterial : api.getTempMaterial;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = getUrl + 'access_token=' + data.access_token;
            if(!permanent) url += '&media_id=' + mediaId;
            resolve(url)
        });
    });
}


Wechat.prototype.delMaterial = function(mediaId){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.delPermMaterial + 'access_token=' + data.access_token;
            var form = {media_id:mediaId}
            request({url:url,method:'POST',formData:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data.errcode === 0){
                    resolve();
                }else{
                    throw new Error('delete permanent material failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.updateMaterial = function(mediaId,news){
    var that = this;
    var form ={
        media_id: mediaId
    }

    _.extend(form, news)

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.updatePermMaterial + 'access_token=' + data.access_token + '&media_id=' + mediaId;
            request({url:url,method:'POST',body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data.errcode === 0){
                    resolve(_data);
                }else{
                    throw new Error('delete permanent material failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.countMaterial = function(){
    var that = this;


    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.count + 'access_token=' + data.access_token;
            request({url:url,method:'GET',json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('Count material failed!');
                }
            })
        });
    });
}

Wechat.prototype.batchMaterial = function(options){
    var that = this;

    options.type = options.type || 'image';
    options.offset = options.type || 0 ;
    options.count = options.type || 1 ;

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.batch + 'access_token=' + data.access_token;
            request({url:url,method:'POST',body:options,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('delete material failed!');
                }
            })
        });
    });
}

Wechat.prototype.reply = function(){
    var content = this.body;
    var message = this.weixin;

    var xml = util.tpl(content,message);

    this.status = 200;
    this.type = 'application/xml';
    this.body = xml;
}





/**************************用户分组Groups******************************/

Wechat.prototype.createGroup = function(name){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.create + 'access_token=' + data.access_token;
            var form = {
                group:{
                    name:name
                }
            };
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('create group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}


Wechat.prototype.fetchGroups = function(name){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.fetch + 'access_token=' + data.access_token;
        
            request({url:url,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('fetch group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}


Wechat.prototype.checkGroup = function(openId){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.check + 'access_token=' + data.access_token;
            var form = {
                openid:openId
            };
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('check by OpenId failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.updateGroup = function(id,name){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.update + 'access_token=' + data.access_token;
            var form = {
                group:{
                    id:id,
                    name:name
                }
            };
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('update group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

/*Wechat.prototype.moveGroup = function(openId,to){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.move + 'access_token=' + data.access_token;
            var form = {
                openid:openId,
                to_groupid:to
            };
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('Move group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}*/

Wechat.prototype.moveGroup = function(openIds,to){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url;
            var form = {
                to_groupid:to
            };
            if(_.isArray(openIds)) {
                url = api.groups.batchupdate + 'access_token=' + data.access_token;
                form.openid_list = openIds
               
            }else{
                url = api.groups.move + 'access_token=' + data.access_token;
                form.openid = openIds
            }
           
           
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('Move group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.deleteGroup = function(id){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.groups.del + 'access_token=' + data.access_token;
            var form = {
                group:{
                    id:id,
                }
            };
           
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('delete group failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.remarkUser = function(openId,remark){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.user.remark + 'access_token=' + data.access_token;
            var form = {
                openid: openId,
                remark: remark
            };
           
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('remarkUser failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.fetchUsers = function(openIds,lang){
    var that = this;
    lang = lang || 'zh_CN';
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){

            var options = {
                json: true
            }
            if(_.isArray(openIds)) {
                options.url = api.user.batchFetch + 'access_token=' + data.access_token;
                options.body = {
                    user_list: openIds
                };
                options.method = 'POST';
            }else{
                options.url = api.user.fetch + 'access_token=' + data.access_token + '&openid='+ openIds +'&lang=' + lang;
            }
           

           
            request(options).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('batchFetch failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.listUsers = function(openId){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.user.list + 'access_token=' + data.access_token;

            if(openId){
                url += '&next_openid=' + openId;
            }

            var form = {
                openid: openId
            };
           
            request({method:'GET',url:url,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('listUsers failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.sendByGroup = function(type,message, groupid){
    var that = this;
    var msg = {
        filter:{},
        msgtype:type
    }
    if(!groupid){
        msg.filter.is_to_all = true
    }else{
        msg.filter.is_to_all = false;
        msg.filter.group_id = groupid;
    }
    msg[type] = message;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.mass.group + 'access_token=' + data.access_token;
            request({method:'POST',url:url,body:msg,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('send mass message failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.sendByOpenId = function(type,message, groupIds){
    var that = this;
    var msg = {
        msgtype: type,
        touser: groupIds
    };

    msg[type] = message;
    
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.mass.openId + 'access_token=' + data.access_token;
            request({method:'POST',url:url,body:msg,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('send by openid failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.deleteMass = function(msgId){
    var that = this;
    
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.mass.del + 'access_token=' + data.access_token;
            var form = {
                msg_id: msgId
            }
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('deleteMass failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.previewMass = function(type,message,openId){
    var that = this;
     var msg = {
        msgtype: type,
        touser: openId
    };

    msg[type] = message;
    
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.mass.preview + 'access_token=' + data.access_token;
         
            request({method:'POST',url:url,body:msg,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('preview mass failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.checkMass = function(msgId){
    var that = this;
    
    
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.mass.check + 'access_token=' + data.access_token;
            var form = {
                msg_id: msgId
            };
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('checkMass failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

//创建菜单
Wechat.prototype.createMenu = function(menu){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.menu.create + 'access_token=' + data.access_token;
            request({url:url,method:'POST',body:menu,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('create menu failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

//获取菜单
Wechat.prototype.getMenu = function(){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.menu.get + 'access_token=' + data.access_token;
            request({url:url,json:true}).then(function(response){
                var _data = response.body;
                if(_data.menu){
                    resolve(_data.menu);
                }else{
                    throw new Error('get menu failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

//删除菜单
Wechat.prototype.deleteMenu = function(){
    var that = this;
    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.menu.del + 'access_token=' + data.access_token;
            request({url:url,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('delete menu failed!');
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.getCurrentMenu = function(){
    var that = this;

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.menu.current + 'access_token=' + data.access_token;
            request({url:url,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('getCurrentMenu failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.createQrcode = function(qr){
    var that = this;

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.qrcode.create + 'access_token=' + data.access_token;
            request({method:'POST',url:url,body:qr,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('createQrcode failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.showQrcode = function(ticket){
    return api.qrcode.show = 'ticket=' + encodeURL(ticket);
}

Wechat.prototype.createShorturl = function(action , url){
    action = action || 'long2short'
    var that = this;

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.qrcode.create + 'access_token=' + data.access_token;
            var form = {
                action: action,
                long_url : url
            }
            request({method:'POST',url:url,body:form,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('createShorturl failed: ' + _data.errmsg);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

Wechat.prototype.semantic = function(semanticData){
    var that = this;

    return new Promise(function(resolve,reject){
        that.fetchAccessToken().then(function(data){
            var url = api.semanticUrl + 'access_token=' + data.access_token;
            semanticData.appid = data.appID
            request({method:'POST',url:url,body:semanticData,json:true}).then(function(response){
                var _data = response.body;
                if(_data){
                    resolve(_data);
                }else{
                    throw new Error('semantic failed: ' + _data);
                }
            }).catch(function(err){
                reject(err);
            });
        });
    });
}

module.exports = Wechat