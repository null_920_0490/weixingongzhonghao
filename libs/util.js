'use strict'

var fs = require('fs');
var Pomise = require('bluebird');

exports.readFileAsync = function (fpath, encodnig) {
	return new Pomise(function(resolve, reject){
		fs.readFile(fpath, encodnig, function(err,content){
			if(err) reject(err)
			else resolve(content)
		})
	})
}




exports.writeFileAsync = function (fpath, content) {
	return new Pomise(function(resolve, reject){
		fs.writeFile(fpath, content, function(err){
			if(err) reject(err)
			else resolve()
		})
	})
}