'use strict'

var path = require('path');
var config = require('./config');
var Wechat = require('./wechat/wechat');
var menu = require('./menu');


var wechatApi = new Wechat(config.wechat);

wechatApi.deleteMenu().then(function(){
    return wechatApi.createMenu(menu);
}).then(function(msg){
    console.log(msg);
});


function* reply(next){
    var message = this.weixin;

    if(message.MsgType === 'event'){
        if(message.Event === 'subscribe'){
            if(message.EventKey){
             console.log('扫描二维码关注：'+message.EventKey+' '+message.ticket);
            }

            this.body = '终于等到你，还好我没放弃'+message.MsgId;
        }else if(message.Event === 'unsubscribe'){
            console.log(message.FromUserName +' 悄悄地走了...');
            this.body = ''
        }else if(message.Event === 'LOCATION'){
            this.body = '您上报的地理位置是：'+ message.Latitude + ',' + message.Longitude;
        }else if(message.Event === 'CLICK'){
            this.body = '您点击了菜单:' + message.EventKey;
        }else if(message.Event === 'SCAN'){
            this.body = '关注后扫描二维码：'+ message.Ticket;
        }else if(message.Event === 'VIEW'){
            this.body = '您点击了菜单中的链接：'+ message.EventKey;
        }else if(message.Event === 'scancode_push'){
            console.log(message.ScanCodeInfo.ScanType);
            console.log(message.ScanCodeInfo.ScanResult);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }else if(message.Event === 'scancode_waitmsg'){
            console.log(message.ScanCodeInfo.ScanType);
            console.log(message.ScanCodeInfo.ScanResult);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }else if(message.Event === 'pic_sysphoto'){
            console.log(message.SendPicsInfo.PicList);
            console.log(message.SendPicsInfo.Count);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }else if(message.Event === 'pic_photo_or_album'){
            console.log(message.SendPicsInfo.PicList);
            console.log(message.SendPicsInfo.Count);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }else if(message.Event === 'pic_weixin'){
            console.log(message.SendPicsInfo.PicList);
            console.log(message.SendPicsInfo.Count);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }else if(message.Event === 'location_select'){
            console.log(message.SendLocationInfo.Location_X);
            console.log(message.SendLocationInfo.Location_Y);
            console.log(message.SendLocationInfo.Scale);
            console.log(message.SendLocationInfo.Label);
            console.log(message.SendLocationInfo.Poiname);
            console.log(message.SendPicsInfo.Count);
            this.body = '您点击了菜单中：'+ message.EventKey;
        }



    }else if(message.MsgType === 'text'){
        var content = message.Content;
        var reply = '你说的话：“' + content + '”，我听不懂呀';
        if(content === '1'){
            reply = '周洁我爱你！';
        }else if(content === '2'){
           reply = [{
                picUrl:'http://tu.23juqing.com/d/file/html/gndy/dyzz/2017-04-09/da9c7a64ab7df196d08b4b327ef248f2.jpg',
            }];
        }else if(content === '3'){
            reply = [{
                title:'金刚.骷髅岛',
                description:'南太平洋上的神秘岛屿——骷髅岛。史上最大金刚与邪恶骷髅蜥蜴的较量。',
                picUrl:'http://tu.23juqing.com/d/file/html/gndy/dyzz/2017-04-09/da9c7a64ab7df196d08b4b327ef248f2.jpg',
                url:'http://s171204w65.iask.in/'
            },{
                title:'金刚.骷髅岛',
                description:'南太平洋上的神秘岛屿——骷髅岛。史上最大金刚与邪恶骷髅蜥蜴的较量。',
                picUrl:'http://tu.23juqing.com/d/file/html/gndy/dyzz/2017-04-09/da9c7a64ab7df196d08b4b327ef248f2.jpg',
                url:'http://www.piaohua.com/html/dongzuo/2017/0409/31921.html'
            }];
        }else if(content === '4'){
            reply = [{
                title:'金刚.骷髅岛',
                description:'南太平洋上的神秘岛屿——骷髅岛。史上最大金刚与邪恶骷髅蜥蜴的较量。',
                picUrl:'http://tu.23juqing.com/d/file/html/gndy/dyzz/2017-04-09/da9c7a64ab7df196d08b4b327ef248f2.jpg',
                url:'http://www.piaohua.com/html/dongzuo/2017/0409/31921.html'
            }];
        }else if(content === '5'){
          var data = yield wechatApi.uploadPermMaterial('other', __dirname+'/public/king.jpg');
            reply = {
                type:'image',
                mediaId:data.media_id
            }
        }else if(content === '6'){
            var data = yield wechatApi.uploadTempMaterial('voice', __dirname+'/public/aiyou.mp3');
            reply = {
                type:'voice',
                mediaId:data.media_id
            }
        }else if(content === '7'){
            var picData = yield wechatApi.uploadTempMaterial('image', __dirname+'/public/1.jpg',{});

            var meida = {
                articles:[{
                    title:'haojie',
                    thumb_media_id: picData.media_id,
                    author: 'hehaojie',
                    digest: 'DIGEST',
                    show_cover_pic: 1,
                    content: 'CONTENT',
                    content_source_url: 'http://www.piaohua.com/html/dongzuo/2017/0409/31921.html'
                }]
            }

            data = yield wechatApi.uploadPermMaterial('news',meida,{});
            data = yield wechatApi.getMaterial(data.media_id,'news',{});

            var dataItem = data.news_item;

             console.log(dataItem);
            /*var news = [];

            dataItem.forEach(function(item){
                news.push({
                    title:item.title,
                    description:item.digest,
                    picUrl:picData.url,
                    url: item.url
                })
            })*/
           /* for(var i =0;i<dataItem.length;i++){
                news.push({
                    title:dataItem[i].title,
                    description:dataItem[i].digest,
                    picUrl:picData.url,
                    url: dataItem[i].url
                })
            }*/


            reply = "news"
        }else if(content === '8'){
            var counts = yield wechatApi.countMaterial();

            console.log(JSON.stringify(counts));

            var results = yield [
                wechatApi.batchMaterial({
                    type:'image',
                    offset: 0,
                    count: 10
                }),
                wechatApi.batchMaterial({
                    type:'video',
                    offset: 0,
                    count: 10
                }),
                wechatApi.batchMaterial({
                    type:'voice',
                    offset: 0,
                    count: 10
                }),
                wechatApi.batchMaterial({
                    type:'news',
                    offset: 0,
                    count: 10
                }),
            ]

            console.log(results);
            reply = '1'
        }else if(content === '9'){
            var group = yield wechatApi.createGroup('豪杰1');
            console.log("新分组 豪杰");
            console.log(group);

            var groups = yield wechatApi.fetchGroups();
            console.log('加了 豪杰 新分组列表 ');
            console.log(groups);

            var group2 = yield wechatApi.checkGroup(message.FromUserName);
            console.log('查看自己的分组');
            console.log(group2);

            var group3 = yield wechatApi.moveGroup(message.FromUserName,108);
            console.log('移动到 108');
            console.log(group3);

            var group4 = yield wechatApi.fetchGroups();
            console.log('移动后的分组列表 ');
            console.log(group4);

            var group5 = yield wechatApi.moveGroup([message.FromUserName],106);
            console.log('批量移动到 106');
            console.log(group5);

            var group6 = yield wechatApi.fetchGroups();
            console.log('批量移动后的分组列表 ');
            console.log(group6);

            var group7 = yield wechatApi.updateGroup(108,'WeChat108');
            console.log('108 改名 WeChat108');
            console.log(group7);

            var group8 = yield wechatApi.fetchGroups();
            console.log('改名后 ');
            console.log(group8);

            var group9 = yield wechatApi.deleteGroup(104);
            console.log('删除104分组');
            console.log(group9);

            var group10 = yield wechatApi.fetchGroups();
            console.log('删除后 ');
            console.log(group10);

            reply = '在后台查看分组'
        }else if(content === '10'){
            var user = yield wechatApi.fetchUsers(message.FromUserName, 'en');
            console.log(user);


            var openIds = [
                {
                    "openid": message.FromUserName, 
                    "lang": "en"
                }
            ];
            var users = yield wechatApi.fetchUsers(openIds);
            console.log(users);

             reply = JSON.stringify(user)
        }else if(content === '11'){
             var userList = yield wechatApi.listUsers();
             console.log(userList);
             reply = userList.total
        }else if(content === '12'){
             var text = {
                content:'这是群发消息测试唔~'
             };
            var msg = yield wechatApi.sendByGroup('text',text,106);
            console.log('msg:'+ JSON.stringify(msg));

            reply = 'yes'
        }else if(content === '13'){
            var text = {
                content:'这是群发消息测试唔~'
             };
            var msgData = yield wechatApi.previewMass('text',text,'oyY_jwv7JnlB0pPw-jnWOiFwjboA');
            console.log(msgData);

            reply = 'yes'
        }else if(content === '14'){
            var msgData = yield wechatApi.checkMass('1000000001');
            console.log(msgData);

            reply = 'yes'
        }else if(content === '15'){
            var tempQr = {
                expire_seconds: 400000,
                action_name: 'QR_SCENE',
                action_info: {
                    scene: {
                        scene_id: 123
                    }
                }
            };
            var permQr = {
                action_name: 'QR_LIMIT_SCENE',
                action_info: {
                    scene: {
                        scene_id: 123
                    }
                }
            };
            var permStrQr = {
                action_name: 'QR_LIMIT_STR_SCENE',
                action_info: {
                    scene: {
                        scene_str: 'haojie'
                    }
                }
            };

            var qr1 = yield wechatApi.createQrcode(tempQr);
            var qr2 = yield wechatApi.createQrcode(permQr);
            var qr3 = yield wechatApi.createQrcode(permStrQr);

            console.log(qr1);
            console.log(qr2);
            console.log(qr3);

            reply = 'yes'
        }else if(content === '16'){
            var longUrl = 'http://www.baidu.com';

            var shortData = yield wechatApi.createShorturl(null, longUrl);
            reply = shortData.short_url
        }else if(content === '17'){
            var semanticData = {
                query:'易达',
                city:'广州',
                category: 'meijun',
                uid:message.FromUserName
            } 
///////////还没获得只能接口权限
            var _semanticData = yield wechatApi.semantic(semanticData);
            reply = JSON.stringify(_semanticData)
        }



    this.body = reply;
    }

    yield next;
}

exports.reply = reply;

